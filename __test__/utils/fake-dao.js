const fakeDao = (data) => ({
  getAll() {
    return new Promise(function (resolve, reject) {
      if (data) {
        resolve(data);
      } else {
        reject(new Error("Fake error on find"));
      }
    });
  },
  getById(id) {
    return new Promise(function (resolve, reject) {
      if (data) {
        if (id === "validId") {
          resolve(data);
        } else resolve(null);
      } else {
        reject(new Error("Fake error on findById"));
      }
    });
  },

  update(id, payload) {
    return new Promise(function (resolve, reject) {
      if (data) {
        if (id === "validId") {
          resolve(data);
        } else resolve(null);
      } else {
        reject(new Error("Fake error on findById"));
      }
    });
  },
  delete(id) {
    return new Promise(function (resolve, reject) {
      if (data) {
        if (id === "validId") {
          resolve(data);
        } else resolve(null);
      } else {
        reject(new Error("Fake error on findById"));
      }
    });
  },

  create(element) {
    return new Promise(function (resolve, reject) {
      if (data) resolve(data);
      else reject(new Error("fake error on create"));
    });
  },
});

module.exports = fakeDao;
