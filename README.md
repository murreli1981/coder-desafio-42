# coder-desafio-41

Configuración:

.env

## Estructura del proyecto:

```
.
├── __test__
│   ├── __mock__
│   ├── dal
│   │   └── integration
│   ├── services
│   └── utils
├── imgs
├── logs
├── performance
│   └── report
├── src
│   ├── auth
│   ├── config
│   ├── controllers
│   ├── dal
│   │   ├── memory
│   │   │   ├── Dao
│   │   │   └── repositories
│   │   └── mongoose
│   │       ├── Dao
│   │       ├── Dto
│   │       ├── db
│   │       ├── models
│   │       └── repositories
│   ├── graphql
│   │   ├── resolvers
│   │   └── schema
│   ├── resources
│   ├── routes
│   ├── services
│   ├── utils
│   └── views
│       ├── layouts
│       └── partials
└── target
```

## Inicio del server

inicia el servidor con la persistencia de mongo

se puede levantar el servidor de la siguiente manera

`npm run start:prod` --> ["start:prod": "set NODE_ENV=production && node ./src/server.js"]
apunta a la base `chat` y hace un set del NODE_ENV
`npm run start:dev` --> ["start:dev": "set NODE_ENV=development && node ./src/server.js"]
apunta a la base `develop-chat` y hace un set del NODE_ENV
`npm run start:prod-linux` --> ["start:prod-linux": "export NODE_ENV=production && node ./src/server.js"]
apunta a la base `chat` y hace un export del NODE_ENV
`npm run start:dev-linux` --> ["start:dev-linux": "export NODE_ENV=development && node ./src/server.js"]
apunta a la base `develop-chat` y hace un export del NODE_ENV

### url:

`http://localhost:8080/ingreso`

## .ENV

```
-rw-r--r--   1 marcelourreli  staff   538B Nov 25 19:33 .env
-rw-r--r--   1 marcelourreli  staff   121B Nov 25 19:33 development.env
-rw-r--r--   1 marcelourreli  staff   113B Nov 25 19:13 production.env
```

`development.env` variable de entorno MONGO_URI con el valor del connection string de una base de desarrollo
`production.env` variable de entorno MONGO_URI y apunta a una base de producción
`.env` resto de variables de entorno comunes a todo el backend

```javascript
//si no recibe un path desde el process.env
const getEnvFile = () => {
  if (
    process.env.NODE_ENV === "development" ||
    process.env.NODE_ENV === "production"
  ) {
    return process.env.NODE_ENV + ".env";
  } else {
    return "development.env";
  }
};

process.env.NODE_ENV ? process.env.NODE_ENV + ".env" : "development.env";

const { parsed: defaultProps } = require("dotenv").config();
const { parsed: envProps } = require("dotenv").config({ path: getEnvFile() });
const env = { ...defaultProps, ...envProps };



...

```

`src/config.js` importa el dotenv por defecto y uno custom que hace referencia a un nombre que recibe por parámetros.
guardo todas las variables que recupero del `.env` + `<<environment>>.env` en el const `env`

luego se usa el objeto `env` para asignar a todas las keys que se exportan en el `config.js`

### test y coverage

los test se corren con el comando

`npm test`

que referencia a la siguiente tarea en el package.json

`"test": "jest --coverage"`

```
 PASS  __test__/services/product.test.js
  service product unit test
    ✓ getAllProducts(): it should return an array of elements when they exist (5 ms)
    ✓ getAllProducts(): the product returned should contain id, item, price and thumbnail (1 ms)
    ✓ getAllProducts() it should return an empty array when there's no result (3 ms)
    ✓ getProduct() it should return null when a given product is not found (1 ms)
    ✓ getProduct() it should return null when a non-mongo id kind is being sent
    ✓ getProduct() it should return an item with a valid id (4 ms)
    ✓ addProduct() it should return the add information when it's added (3 ms)
    ✓ updateProduct() it should return a valid object after updating (6 ms)
    ✓ deleteProduct() it should return a valid response after deleting a valid object (43 ms)
    ✓ should return undefined when model fails (63 ms)
  dal validation
    ✓ validate singleton implementation for dal (40 ms)



-----------------------------|---------|----------|---------|---------|-------------------
File                         | % Stmts | % Branch | % Funcs | % Lines | Uncovered Line #s
-----------------------------|---------|----------|---------|---------|-------------------
All files                    |   61.32 |    49.01 |      50 |   62.42 |
 __test__/__mock__           |     100 |      100 |     100 |     100 |
  item-found-data.js         |     100 |      100 |     100 |     100 |
  item-stored-data.js        |     100 |      100 |     100 |     100 |
  items-found-data.js        |     100 |      100 |     100 |     100 |
  no-items-found.js          |     100 |      100 |     100 |     100 |
 __test__/utils              |    93.1 |     87.5 |     100 |   92.59 |
  fake-dao.js                |    93.1 |     87.5 |     100 |   92.59 | 28,39
 src/config                  |     100 |    58.82 |     100 |     100 |
  globals.js                 |     100 |    58.82 |     100 |     100 | 9-16
 src/dal                     |   41.66 |     12.5 |     100 |      50 |
  index.js                   |   41.66 |     12.5 |     100 |      50 | 11-14
 src/dal/memory              |     100 |      100 |     100 |     100 |
  index.js                   |     100 |      100 |     100 |     100 |
 src/dal/memory/Dao          |   20.83 |        0 |    12.5 |   19.04 |
  index.js                   |     100 |      100 |     100 |     100 |
  productDao.js              |   11.53 |        0 |   11.11 |    8.33 | 3-43
  userDao.js                 |   16.66 |        0 |   14.28 |   14.28 | 3-27
 src/dal/memory/repositories |    30.3 |        0 |      20 |   26.66 |
  baseRepository.js          |   11.53 |        0 |   11.11 |    8.33 | 3-43
  index.js                   |     100 |      100 |     100 |     100 |
  messageRepository.js       |     100 |      100 |     100 |     100 |
 src/services                |     100 |      100 |     100 |     100 |
  product.js                 |     100 |      100 |     100 |     100 |
 src/utils                   |     100 |      100 |     100 |     100 |
  loggers.js                 |     100 |      100 |     100 |     100 |
-----------------------------|---------|----------|---------|---------|-------------------
```
