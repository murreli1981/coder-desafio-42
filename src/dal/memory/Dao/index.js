const models = require("../../../../__test__/__mock__/init-memory.json");
const userDao = require("./userDao");
const productDao = require("./productDao");

module.exports = {
  userDao: userDao(models),
  productDao: productDao(models),
};
