const { STORAGE } = require("../config/globals");

/**
 * @description Método factory para seleccionar el tipo de persistencia
 * si opt no existe toma el parámetro que llega desde storage
 * @param {String} opt
 * @returns
 */
const factory = (opt) => {
  if (opt === "MEMORY") return require("./memory");
  else if (opt === "MONGO") return require("./mongoose");
  else if (STORAGE === "MEMORY") return require("./memory");
  else if (STORAGE === "MONGO") return require("./mongoose");
  else return require("./memory");
};

module.exports = factory;
