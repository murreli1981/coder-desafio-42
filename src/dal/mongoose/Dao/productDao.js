const productDao = ({ productModel }) => ({
  async getAll(username) {
    const products = await productModel.find();
    return products;
  },

  async getById(id) {
    const product = await productModel.findById(id);
    return product;
  },

  async create(product) {
    const productCreated = await productModel.create(product);
    return productCreated;
  },

  async delete(id) {
    const productDeleted = await productModel.findByIdAndDelete(id);
    return productDeleted;
  },

  async update(id, payload) {
    const productUpdated = await productModel.findByIdAndUpdate(id, payload);
    return productUpdated;
  },
});

module.exports = productDao;
