const userDao = ({ userModel }) => ({
  async getByUsername(username) {
    const user = await userModel.findOne({ username });
    return user;
  },

  async getById(id) {
    const user = await userModel.findById(id);
    return user;
  },

  async create(user) {
    const userCreated = await userModel.create(user);
    console.log(userCreated);
    return userCreated;
  },

  async findOrCreate(newUser) {
    const user = await userModel.findOrCreate(newUser);
    return user;
  },
});

module.exports = userDao;
