const models = require("../models");
const messageRepository = require("./messageRepository");

module.exports = {
  messageRepository: messageRepository(models),
};
