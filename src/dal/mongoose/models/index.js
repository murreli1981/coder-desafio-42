const productModel = require("./product");
const messageModel = require("./message");
const userModel = require("./user");

module.exports = {
  productModel,
  messageModel,
  userModel,
};
