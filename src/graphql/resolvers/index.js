const services = require("../../services");
const productResolver = require("./productResolver");

module.exports = {
  productResolver: productResolver(services),
};
