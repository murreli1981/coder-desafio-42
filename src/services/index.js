const { STORAGE_FROM_ARGS } = require("../config/globals");
const productService = require("./product");
const userService = require("./user");
const messageService = require("./message");
let persistence = require("../dal")();

module.exports = {
  productService: productService(persistence),
  userService: userService(persistence),
  messageService: messageService(persistence),
};
