const inputBtn = document.querySelector("#prodInputBtn");

inputBtn.addEventListener("click", (e) => {
  e.preventDefault();
  const title = document.querySelector("#title");
  const price = document.querySelector("#price");
  const thumbnail = document.querySelector("#thumbnail");

  const query2 = `mutation {
                            createProduct(
                              product: {
                                title: "${title.value}"
                                price: "${price.value}"
                                thumbnail: "${thumbnail.value}"
                              }
                            ) {
                              _id
                              title
                              price
                              thumbnail
                            }
                          }`;
  fetch("/graphql?", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify({
      query: query2,
    }),
  })
    .then((response) => response.json())
    .then((data) => {
      console.log("added");
      console.log(data);
      console.log(query2);
      window.location.reload();
    })
    .catch((err) => {
      console.log("error");
      console.log(err);
      console.log(query2);
    });
});
