//si no recibe development o production desde el process.env devuelve por default
const getEnvFile = () => {
  if (
    process.env.NODE_ENV === "development" ||
    process.env.NODE_ENV === "production"
  ) {
    return process.env.NODE_ENV + ".env";
  } else {
    return "development.env";
  }
};

process.env.NODE_ENV ? process.env.NODE_ENV + ".env" : "development.env";

const { parsed: defaultProps } = require("dotenv").config();
const { parsed: envProps } = require("dotenv").config({ path: getEnvFile() });
const env = { ...defaultProps, ...envProps };

const STORAGE_FROM_ARGS = process.argv[2];
const SERVER_MODE_FROM_ARGS = process.argv[3];
const PORT_FROM_ARGS = process.argv[4];
const FB_CLIENT_ID_FROM_ARGS = process.argv[5];
const FB_CLIENT_SECRET_FROM_ARGS = process.argv[6];

console.info({ "db-string": env.MONGO_URI });
module.exports = {
  STORAGE: STORAGE_FROM_ARGS ? STORAGE_FROM_ARGS.toUpperCase() : "MONGO",
  PORT: PORT_FROM_ARGS || env.PORT || 8080,
  MONGO_URI: env.MONGO_URI || "undefined",
  SECRET_KEY: env.SECRET_KEY || "qwertyuiop",
  FB_CLIENT_ID: FB_CLIENT_ID_FROM_ARGS || env.FB_CLIENT_ID || "id",
  FB_CLIENT_SECRET:
    FB_CLIENT_SECRET_FROM_ARGS || env.FB_CLIENT_SECRET || "secret",
  IS_CLUSTER: SERVER_MODE_FROM_ARGS === "CLUSTER" ? true : false,
  LOG_FILE_PATH: env.LOG_FILE_PATH,
  ACCOUNT_TWILIO_SID: env.ACCOUNT_TWILIO_SID,
  TWILIO_AUTH_TOKEN: env.TWILIO_AUTH_TOKEN,
  ADMIN_PHONE_NUMBER: env.ADMIN_PHONE_NUMBER,
  FROM_PHONE_NUMBER: env.FROM_PHONE_NUMBER,
  NOTIFICATIONS: {
    ETHEREAL: {
      host: "smtp.ethereal.email",
      port: 587,
      secure: false,
      auth: {
        user: env.ETHEREAL_EMAIL,
        pass: env.ETHEREAL_PASS,
      },
    },
    GMAIL: {
      host: "smtp.gmail.com",
      port: 465,
      secure: true,
      auth: {
        user: env.GMAIL_EMAIL,
        pass: env.GMAIL_PASS,
      },
    },
  },
};
